/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import de.samply.mdr.dao.Vocabulary;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Message;
import de.samply.sdao.json.JSONResource;
import de.samply.sdao.json.Value;
import de.samply.string.util.StringUtil;

/**
 * Some validators that validate definitions, slots, permissible values, etc.
 * @author paul
 *
 */
public class BeanValidators {

    /**
     * Disable instantiation.
     */
    private BeanValidators() {
    }

    /**
     * Validated the given value domain.
     * @param valueDomain
     * @return
     */
    public static boolean validateValidation(ValueDomainDTO valueDomain) {
        boolean valid = true;

        switch(valueDomain.getDatatype()) {
        case LIST:
            /**
             * Check if every value is unique and not empty.
             */
            HashSet<String> permittedValues = new HashSet<>();
            for(AdapterPermissibleValue v : valueDomain.getPermittedValues()) {
                if(permittedValues.contains(v.getValue())) {
                    Message.addError("duplicateValue");
                    valid = false;
                } else {
                    permittedValues.add(v.getValue());
                }

                if(StringUtil.isEmpty(v.getValue())) {
                    Message.addError("emptyValue");
                    valid = false;
                }

                valid = BeanValidators.validateDefinitions(v.getDefinitions()) ? valid : false;
            }
            break;

        case INTEGER:
            /**
             * Check if the range make sense.
             */
            if(valueDomain.getWithinRange() && valueDomain.getUseRangeFrom() && valueDomain.getUseRangeTo()) {
                if(!((valueDomain.getRangeFrom() instanceof Long) && (valueDomain.getRangeTo() instanceof Long))
                        && !((valueDomain.getRangeFrom() instanceof BigDecimal) && (valueDomain.getRangeTo() instanceof BigDecimal))
                        && !((valueDomain.getRangeFrom() instanceof BigInteger) && (valueDomain.getRangeTo() instanceof BigInteger))) {
                    Message.addError("invalidInteger");
                    valid = false;
                }

                /**
                 * In Mojarra the NumberConverter creates a BigDecimal, even if the user entered "100".
                 * Convert it to a long
                 */
                if(valueDomain.getRangeFrom() instanceof BigDecimal && valueDomain.getRangeTo() instanceof BigDecimal) {
                    valueDomain.setRangeFrom(((BigDecimal)valueDomain.getRangeFrom()).toBigInteger());
                    valueDomain.setRangeTo(((BigDecimal)valueDomain.getRangeTo()).toBigInteger());
                }

                if(valueDomain.getRangeFrom().longValue() >= valueDomain.getRangeTo().longValue()) {
                    Message.addError("invalidRange");
                    valid = false;
                }
            }
            break;

        case FLOAT:
            /**
             * Check if the range makes sense
             */
            if(valueDomain.getWithinRange() && valueDomain.getUseRangeFrom() && valueDomain.getUseRangeTo()) {
                if(valueDomain.getRangeFrom().doubleValue() >= valueDomain.getRangeTo().doubleValue()) {
                    Message.addError("invalidRange");
                    valid = false;
                }
            }
            break;

        case STRING:
            /**
             * Try to parse the regular expression. If it fails it is not a valid regex.
             */
            if(valueDomain.getUseRegex()) {
                try {
                    Pattern.compile(valueDomain.getFormat());
                } catch(Exception e) {
                    Message.addError("invalidRegex");
                    valid = false;
                }
            }
            break;

        case CATALOG:
            if(StringUtil.isEmpty(valueDomain.getCatalog())) {
                Message.addError("catalogNotSelected");
                valid = false;
            }

        default:
            return true;
        }

        return valid;
    }

    /**
     * Validates the list of definitions.
     * @param definitions
     * @return
     */
    public static boolean validateDefinitions(List<AdapterDefinition> definitions) {
        boolean valid = true;

        if(definitions.size() == 0) {
            Message.addError("noDefinitions");
            valid = false;
        }

        for(AdapterDefinition d : definitions) {
            if(StringUtil.isEmpty(d.getDefinition())) {
                Message.addError("emptyDefinition");
                valid = false;
            }

            if(StringUtil.isEmpty(d.getDesignation())) {
                Message.addError("emptyDesignation");
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Validates the given list of slots. Empty slots are removed with this operation.
     * @param slots list of slots that are verified in this method
     * @return true if all slots are valid, false otherwise
     */
    public static boolean validateSlots(List<SlotDTO> slots) {
        boolean valid = true;

        Iterator<SlotDTO> iterator = slots.iterator();

        while(iterator.hasNext()) {
            SlotDTO slot = iterator.next();

            if(StringUtil.isEmpty(slot.getName()) && StringUtil.isEmpty(slot.getValue())) {
                iterator.remove();
                continue;
            }

            if(StringUtil.isEmpty(slot.getName())) {
                Message.addError("emptyName");
                valid = false;
            }

            if(StringUtil.isEmpty(slot.getValue())) {
                Message.addError("emptyValue");
                valid = false;
            }
        }

        return valid;
    }

    /**
     * Checks if the definitions are valid for the given namespace
     * constraints.
     * @param constraints the namespace constraints
     * @param definitions the list of definitions
     * @return True if the definitions are valid. False otherwise
     */
    public static boolean validateConstraints(JSONResource constraints,
            List<AdapterDefinition> definitions) {

        boolean valid = true;

        for(Value language : constraints.getProperties(Vocabulary.LANGUAGES)) {
            boolean found = false;
            for(AdapterDefinition definition : definitions) {
                if(definition.getLanguage().toString().equals(language.getValue())) {
                    found = true;
                }
            }

            if(! found) {
                Message.addError("languageMissing", JSFUtil.getString(language.getValue()));
                valid = false;
            }
        }

        return valid;
    }

    /**
     * Checks if the slots are valid for the given namespace constraints.
     * @param constraints the namespace constraints
     * @param slots list of slots
     * @return True if the slots are valid. False otherwise.
     */
    public static boolean validateConstraintsSlots(JSONResource constraints, List<SlotDTO> slots) {

        boolean valid = true;

        for(Value slotValue : constraints.getProperties(Vocabulary.SLOTS)) {
            boolean found = false;
            for(SlotDTO slot : slots) {
                if(slot.getName().equals(slotValue.getValue())) {
                    found = true;
                }
            }

            if(! found) {
                Message.addError("slotMissing", slotValue.getValue());
                valid = false;
            }
        }

        return valid;
    }

    /**
     * Checks if the permissible value is valid for the given namespace constraints.
     * @param constraints namespace constraints
     * @param value the permissible value
     * @return True if the permissible value is valid. False otherwise.
     */
    public static boolean validateValueConstraints(JSONResource constraints, AdapterPermissibleValue value) {

        boolean valid = true;

        for(Value language : constraints.getProperties(Vocabulary.LANGUAGES)) {
            boolean found = false;

            for(AdapterDefinition definition : value.getDefinitions()) {
                if(definition.getLanguage().toString().equals(language.getValue())) {
                    found = true;
                }
            }

            if(!found) {
                Message.addError("valueLanguageMissing", value.getValue(), JSFUtil.getString(language.getValue()));
                valid = false;
            }
        }

        return valid;
    }

}
