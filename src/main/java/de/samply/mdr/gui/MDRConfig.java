/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.security.PublicKey;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.xml.sax.SAXException;

import de.samply.auth.client.jwt.KeyLoader;
import de.samply.common.config.OAuth2Client;
import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.Vocabulary;
import de.samply.sdao.DAOException;
import de.samply.sdao.Upgrader;
import de.samply.sdao.json.JSONResource;

/**
 * The static configuration class that stores all configuration values
 * of this application.
 * @author paul
 *
 */
public class MDRConfig {

    private static final Logger logger = LogManager.getLogger(MDRConfig.class);

    /**
     * The default project name. This is also the *default* prefix for Samply Common Config.
     */
    public static final String DEFAULT_PROJECT_NAME = "samply";

    /**
     * The *current* project name. This is also the prefix for Samply Common Config.
     * This value should not be changed unless the application has not been initialized yet.
     */
    private static String projectName = "samply";

    /**
     * The configuration file for the postgresql database.
     */
    public static final String FILE_POSTGRES = "mdr.postgres.xml";

    /**
     * The configuration file for the OAuth2 configuration.
     */
    public static final String FILE_OAUTH = "mdr.oauth2.xml";

    /**
     * The configuration file for the log4j2 configuration.
     */
    public static final String FILE_LOG4J2 = "log4j2.xml";

    /**
     * The OAuth2 client configuration.
     */
    private static OAuth2Client oauth2;

    /**
     * The identity providers public key
     */
    private static PublicKey publicKey;

    /**
     * The database credentials.
     */
    private static Postgresql psql;

    /**
     * The *current* database version.
     */
    private static int dbVersion = 0;

    /**
     * If true the server is in maintenance mode:
     * REST interface returns 503, web interface redirects to /maintenance.xhtml or /admin/maintenance.xhtml
     */
    private static Boolean maintenanceMode = false;

    /**
     * If true new users can create new namespaces without explicit permission to do so.
     */
    private static Boolean namespaceGrant = false;

    /**
     * If true new users can create catalogs without explicit permission to do so.
     */
    private static Boolean catalogsGrant = false;

    /**
     * If true new users can use the export and import
     */
    private static boolean exportImportGrant;

    /**
     * If true, a button to translate definitions via external service is added.
     */
    private static boolean showTranslateDefinitionButton;

    public static void initialize(String fallback) throws FileNotFoundException, JAXBException, SAXException,
            ParserConfigurationException, DAOException {

        Configurator.initialize(MDRConfig.class.getCanonicalName(),
                FileFinderUtil.findFile(FILE_LOG4J2, projectName, fallback).getAbsolutePath());


        psql = JAXBUtil.findUnmarshall(FILE_POSTGRES, getJAXBContext(), Postgresql.class, projectName, fallback);
        oauth2 = JAXBUtil.findUnmarshall(FILE_OAUTH, getJAXBContext(), OAuth2Client.class, projectName, fallback);
        publicKey = KeyLoader.loadKey(oauth2.getHostPublicKey());

        logger.debug("Establishing first connection to check the db version");

        try(MDRConnection mdr = ConnectionFactory.get(0)) {
            if(!mdr.isInstalled()) {
                logger.info("Database is not installed correctly or at all. Attempting to create the tables");

                InputStreamReader reader = new InputStreamReader(MDRConnection.class.getResourceAsStream("/sql/mdr.sql"));
                mdr.executeStream(reader);

                Upgrader upgrade = new Upgrader(mdr);
                upgrade.upgrade(false);

                mdr.commit();
            }

            reloadConfig(mdr);
        }
    }

    /**
     * Reloads the configuration from the database. Checks if the database version
     * is equal to the version required to run this application properly.
     * @param auth
     * @throws DAOException
     */
    public static void reloadConfig(MDRConnection auth) throws DAOException {
        JSONResource config = auth.getConfig();
        dbVersion = config.getProperty(Vocabulary.DB_VERSION).asInteger();

        if(dbVersion != MDRConnection.requiredVersion) {
            logger.error("The database versions are not compatible. Starting in maintenance mode. Required Version: "
                    + MDRConnection.requiredVersion + ", current version: " + dbVersion);
            maintenanceMode = true;
            return;
        } else {
            maintenanceMode = false;

            namespaceGrant = config.getProperty(Vocabulary.DEFAULT_NAMESPACE_GRANT).asBoolean();
            catalogsGrant = config.getProperty(Vocabulary.DEFAULT_CATALOG_GRANT).asBoolean();
            exportImportGrant = config.getProperty(Vocabulary.DEFAULT_EXPORT_IMPORT_GRANT).asBoolean();
            showTranslateDefinitionButton = config.getProperty(Vocabulary.SHOW_TRANSLATION_BUTTON).asBoolean();
        }
    }

    private static JAXBContext jaxbContext;


    /**
     * Returns the JAXBContext for Postgresql and OAuth2Client classes. Creates a new one if necessary.
     * @return
     * @throws JAXBException
     */
    private synchronized static JAXBContext getJAXBContext() throws JAXBException {
        if(jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        }
        return jaxbContext;
    }

    /**
     * @return the publicKey
     */
    public static PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * @return the psql
     */
    public static Postgresql getPsql() {
        return psql;
    }

    /**
     * @return the dbVersion
     */
    public static int getDbVersion() {
        return dbVersion;
    }

    /**
     * @return the oauth2
     */
    public static OAuth2Client getOauth2() {
        return oauth2;
    }

    /**
     * @return the maintenanceMode
     */
    public static Boolean getMaintenanceMode() {
        return maintenanceMode;
    }

    /**
     * @return the namespaceGrant
     */
    public static Boolean getNamespaceGrant() {
        return namespaceGrant;
    }

    /**
     * @return the catalogsGrant
     */
    public static Boolean getCatalogsGrant() {
        return catalogsGrant;
    }

    /**
     * @return the projectName
     */
    public static String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public static void setProjectName(String projectName) {
        MDRConfig.projectName = projectName;
    }

    /**
     * @return
     */
    public static boolean getExportImportGrant() {
        return exportImportGrant;
    }

    public static boolean isShowTranslateDefinitionButton() {
        return showTranslateDefinitionButton;
    }
}
