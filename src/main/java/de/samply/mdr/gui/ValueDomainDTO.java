/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.util.HashMap;
import java.util.List;

import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.lib.AdapterValueDomain;
import de.samply.mdr.util.Mapper;

/**
 * The internal value domain. This class combines many different value domains, e.g.
 * a value domain with an integer range or a value domain with a list of
 * permissible values.
 * @author paul
 *
 */
public class ValueDomainDTO extends AdapterValueDomain {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The URN of the selected catalog.
     */
    private String catalog;

    /**
     * The list of all subcodes
     */
    private List<MDRElement> subCodes;

    /**
     * The catalog root TreeNode
     */
    private TreeNode catalogRoot = null;

    /**
     * The hashmap index for tree nodes, the scoped identifier ID is the key.
     */
    private HashMap<Integer, TreeNode> index = null;

    /**
     * The hashmap index for tree nodes, the URN is the key.
     */
    private HashMap<String, TreeNode> urnIndex = null;

    /**
     * Returns the CSS-Class for the given URN. Used in catalogs.
     * @param urn
     * @return
     */
    public String getState(String urn) {
        switch(urnIndex.get(urn).getState()) {
            case ALL:
                return "fa-check-square-o";

            case SOME:
                return "fa-minus-square-o";

            default:
                return "fa-square-o";
        }
    }

    /**
     * Updates the entire tree states.
     */
    public void updateTreeState() {
        for(TreeNode node : catalogRoot.getChildren()) {
            updateState(node);
        }
    }

    /**
     * Updates the given TreeNode status. Recursive function.
     * @param node
     * @return
     */
    private ChildrenSelectionState updateState(TreeNode node) {
        node.setState((node.isSelected() ? ChildrenSelectionState.ALL : ChildrenSelectionState.SOME));
        int checked = 0;

        for(TreeNode child : node.getChildren()) {
            ChildrenSelectionState state = updateState(child);
            if(state != ChildrenSelectionState.ALL && node.getState() == ChildrenSelectionState.ALL) {
                node.setState(ChildrenSelectionState.SOME);
            }

            if(state != ChildrenSelectionState.NONE) {
                ++checked;
            }
        }

        if(node.getState() == ChildrenSelectionState.SOME && checked == 0) {
            node.setState(ChildrenSelectionState.NONE);
        }

        return node.getState();
    }

    public String getNumericRange() {
        if(withinRange) {
            ValueDomain convert = Mapper.convert(this);
            return convert.getFormat();
        } else {
            return "";
        }
    }

    /**
     * @return the catalog
     */
    public String getCatalog() {
        return catalog;
    }

    /**
     * @param catalog the catalog to set
     */
    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    /**
     * @return the root
     */
    public TreeNode getCatalogRoot() {
        return catalogRoot;
    }

    /**
     * @param root the root to set
     */
    public void setCatalogRoot(TreeNode root) {
        this.catalogRoot = root;
    }

    /**
     * @return the subCodes
     */
    public List<MDRElement> getSubCodes() {
        return subCodes;
    }

    /**
     * @param subCodes the subCodes to set
     */
    public void setSubCodes(List<MDRElement> subCodes) {
        this.subCodes = subCodes;
    }

    /**
     * @return the index
     */
    public HashMap<Integer, TreeNode> getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(HashMap<Integer, TreeNode> index) {
        this.index = index;
    }

    /**
     * @return the urnIndex
     */
    public HashMap<String, TreeNode> getUrnIndex() {
        return urnIndex;
    }

    /**
     * @param urnIndex the urnIndex to set
     */
    public void setUrnIndex(HashMap<String, TreeNode> urnIndex) {
        this.urnIndex = urnIndex;
    }

}
