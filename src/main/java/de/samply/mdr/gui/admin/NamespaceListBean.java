/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.UserDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * This bean manages the list of all namespaces in the admin interface.
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class NamespaceListBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of namespaces.
     */
    private List<MDRNamespace> namespaces;

    @PostConstruct
    public void initialize() {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            namespaces = Mapper.convertNamespaces(mdr.get(NamespaceDAO.class).getNamespaces(), Language.EN);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the creator of the given namespace.
     * @param namespace
     * @return
     */
    public String getCreator(Namespace namespace) {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            User user = mdr.get(UserDAO.class).getUser(namespace.getCreatedBy());
            return user.getEmail() + " (" + (user.getExternalLabel() == null ? "Local" : user.getExternalLabel())+ ")";
        } catch (DAOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Toggles the namespaces visibility.
     * @param namespace
     * @return
     */
    public String toggle(Namespace namespace) {
        try(MDRConnection mdr = ConnectionFactory.get()) {
            if(namespace.getHidden()) {
                namespace.setHidden(false);
            } else {
                namespace.setHidden(true);
            }

            mdr.get(NamespaceDAO.class).updateNamespace(namespace);
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the namespaces
     */
    public List<MDRNamespace> getNamespaces() {
        return namespaces;
    }

    /**
     * @param namespaces the namespaces to set
     */
    public void setNamespaces(List<MDRNamespace> namespaces) {
        this.namespaces = namespaces;
    }

}
