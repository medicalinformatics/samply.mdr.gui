/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.rest.UserDTO;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.UserDAO;
import de.samply.mdr.dao.Vocabulary;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRConfig;
import de.samply.mdr.gui.NamespacePermissionDTO;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.sdao.DAOException;
import de.samply.sdao.json.JSONResource;
import de.samply.sdao.json.Value;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * This wizard allows *basic* changes only, that means:
 *
 * - labels (also the labels for permitted values)
 * - permissions
 * - hidden flag
 *
 * @author paul
 *
 */
@SessionScoped
@ManagedBean
public class ChangeNamespaceWizard extends DataElementGroupWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<WizardStep> steps;

    /**
     * The list of the current namespace permissions
     */
    private List<NamespacePermissionDTO> permissions;

    /**
     * If true, the namespace will be hidden.
     */
    private Boolean hidden;

    /**
     * The namespace which is being edited.
     */
    private Namespace ns;

    /**
     * The selected user identity.
     */
    private String identity;

    /**
     * The selected permission for the selected user.
     */
    private Permission permission;

    /**
     * A list of suggestions
     */
    private String suggestions;

    /**
     * The search text from the select2 drop down
     */
    private String searchText;

    /**
     * The list of users that have been loaded. Without this field the
     * database has to be queried every time.
     */
    private List<UserDTO> users;

    /**
     * The required languages.
     */
    private List<Language> languages;

    /**
     * The required slots.
     */
    private List<String> constraintSlots;

    /**
     * The selected language that will be added to the list of required languages.
     */
    private Language language;

    /**
     * The selected slot that will be added to the list of required slots.
     */
    private String slot;

    @Override
    public String validateSlots() {
        return null;
    }

    @Override
    public String validateDefinitions() {
        setStarted(true);

        if(BeanValidators.validateDefinitions(getDefinitions())) {
            return "changenspermissions?faces-redirect=true";
        } else {
            return null;
        }
    }

    /**
     * In case we need to validate the permissions, do it here.
     * @return
     */
    public String validatePermissions() {
        return "changensconstraints?faces-redirect=true";
    }

    /**
     * In case we need to verify the constraints, do it here.
     * @return
     */
    public String validateConstraints() {
        return "changensverify?faces-redirect=true";
    }

    @Override
    public ElementType getElementType() {
        return ElementType.DATAELEMENT;
    }

    @Override
    public String finish() {
        String vDefinitions = validateDefinitions();

        if(vDefinitions == null) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            JSONResource constraints = new JSONResource();

            /**
             * The constaints are saved in the data field.
             */
            for(Language language : languages) {
                constraints.addProperty(Vocabulary.LANGUAGES, language.toString());
            }

            for(String slot : constraintSlots) {
                constraints.addProperty(Vocabulary.SLOTS, slot);
            }
            ns.getData().setProperty(Vocabulary.CONSTRAINTS, constraints);

            List<Definition> target = new ArrayList<Definition>();
            for(AdapterDefinition def : getDefinitions()) {
               target.add(Mapper.convert(def));
            }
            ns.setHidden(hidden);

            mdr.get(NamespaceDAO.class).updateNamespace(ns, target);

            mdr.updatePermissions(Mapper.convertPermissions(permissions), ns);

            mdr.commit();

            /**
             * Reload the namespaces.
             */
            getUserBean().reloadNamespaces(mdr);

            return "/view?faces-redirect=true&namespace=" + ns.getName();
        } catch (DAOException e) {
            e.printStackTrace();
            Message.addError("serverError");
            return null;
        }
    }

    /**
     * Initializes the wizard. Creates all steps and gets the current permissions
     * from the database.
     *
     * @param namespace the current namespace name
     * @return
     */
    public String initialize(String namespace) {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            DescribedElement desc = mdr.get(NamespaceDAO.class).getNamespace(namespace);

            ns = (Namespace) desc.getElement();

            steps = new ArrayList<>();
            steps.add(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/changensdefinitions.xhtml"));
            steps.add(new WizardStep(JSFUtil.getString("permissions"), 2, "/user/changenspermissions.xhtml"));
            steps.add(new WizardStep(JSFUtil.getString("constraints"), 3, "/user/changensconstraints.xhtml"));
            steps.add(new WizardStep(JSFUtil.getString("verification"), 4, "/user/changensverify.xhtml"));

            setDefinitions(Mapper.convert(desc.getDefinitions()));

            permissions = Mapper.convert(mdr.getPermissions(ns), mdr);

            hidden = ns.getHidden();
            setIdentifier(ns.getName());

            languages = new ArrayList<>();
            constraintSlots = new ArrayList<>();

            JSONResource constraints = ns.getConstraints();

            for(Value value : constraints.getProperties(Vocabulary.LANGUAGES)) {
                languages.add(Language.valueOf(value.getValue()));
            }

            for(Value value : constraints.getProperties(Vocabulary.SLOTS)) {
                constraintSlots.add(value.getValue());
            }

        } catch (DAOException e) {
            e.printStackTrace();
            reset();
            return null;
        }
        return "/user/changensdefinitions.xhtml?faces-redirect=true";
    }

    /**
     * Adds the current permission
     * @return
     */
    public String add() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            UserDTO userDTO = null;
            for(UserDTO u : users) {
                if(u.getId().equals(identity)) {
                    userDTO = u;
                }
            }

            /**
             * Something went wrong. Abort.
             */
            if(userDTO == null) {
                return null;
            }

            User user = mdr.get(UserDAO.class).getUserByIdentity(identity);

            /**
             * If the user has not been to the MDR yet, create him.
             */
            if(user == null) {
                user = mdr.get(UserDAO.class).createDefaultUser(identity, userDTO.getEmail(), userDTO.getName(),
                        userDTO.getExternalLabel(), MDRConfig.getNamespaceGrant(), MDRConfig.getCatalogsGrant(), MDRConfig.getExportImportGrant());
                mdr.commit();
            }

            /**
             * Search for duplicates.
             */
            for(NamespacePermissionDTO p : permissions) {
                if(p.getUserId() == user.getId()) {
                    return null;
                }
            }

            NamespacePermissionDTO dto = new NamespacePermissionDTO();
            dto.setEmail(user.getEmail());
            dto.setRealName(user.getRealName());
            dto.setUserId(user.getId());
            dto.setExternalLabel(user.getExternalLabel());
            dto.setPermission(permission);
            permissions.add(dto);

            identity = "";
            permission = Permission.READ;
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Searches the database for a specific user. Uses the username and email attributes.
     * @return
     */
    public String searchEmail() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            users = getUserBean().getAuthClient().searchUser(searchText).getUsers();
            suggestions = StringUtil.join(users, ";|;", new Builder<UserDTO>() {
                @Override
                public String build(UserDTO o) {
                    return o.getName() + " <" + o.getEmail() + "> [" + o.getId() + "]" +
                            (StringUtil.isEmpty(o.getExternalLabel()) ? "" : " (" + o.getExternalLabel() + ")");
                }
            });
        } catch (DAOException | InvalidTokenException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String reset() {
        languages = new ArrayList<>();
        constraintSlots = new ArrayList<>();

        if(ns == null) {
            return "/index?faces-redirect=true";
        } else {
            return "/view?faces-redirect=true&namespace=" + ns.getName();
        }
    }

    /**
     * Adds the current language to the list of required languages.
     * @return
     */
    public String addLanguage() {
        if(! languages.contains(language)) {
            languages.add(language);
        }
        return null;
    }

    /**
     * Removes the given language from the list of required languages.
     * @param language
     * @return
     */
    public String removeLanguage(Language language) {
        languages.remove(language);
        return null;
    }

    /**
     * Adds the current slot to the list of required slots.
     * @return
     */
    public String addSlotConstraint() {
        if(!constraintSlots.contains(slot)) {
            constraintSlots.add(slot);

            slot = "";
        }
        return null;
    }

    /**
     * Not used.
     */
    @Override
    public String validateMembers() {
        throw new UnsupportedOperationException();
    }

    /**
     * Removes the given slot from the list of required slots.
     * @param s
     * @return
     */
    public String removeSlotConstraint(String s) {
        constraintSlots.remove(s);
        return null;
    }

    @Override
    public List<WizardStep> getSteps() {
        return steps;
    }

    @Override
    public Boolean getChange() {
        return true;
    }

    @Override
    public Boolean getReleaseDraftButtons() {
        return false;
    }

    /**
     * @return the hidden
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     * @param hidden the hidden to set
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return the permissions
     */
    public List<NamespacePermissionDTO> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(List<NamespacePermissionDTO> permissions) {
        this.permissions = permissions;
    }

    public void remove(NamespacePermissionDTO permission) {
        permissions.remove(permission);
    }

    /**
     * @return the email
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * @param email the email to set
     */
    public void setIdentity(String email) {
        this.identity = email;
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    /**
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * @return the users
     */
    public List<UserDTO> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    /**
     * @return the languages
     */
    public List<Language> getLanguages() {
        return languages;
    }

    /**
     * @param languages the languages to set
     */
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    /**
     * @return the language
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /**
     * @return the slot
     */
    public String getSlot() {
        return slot;
    }

    /**
     * @param slot the slot to set
     */
    public void setSlot(String slot) {
        this.slot = slot;
    }

    /**
     * @return the constraintSlots
     */
    public List<String> getConstraintSlots() {
        return constraintSlots;
    }

    /**
     * @param constraintSlots the constraintSlots to set
     */
    public void setConstraintSlots(List<String> constraintSlots) {
        this.constraintSlots = constraintSlots;
    }

}
