/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.utils.Exporter;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.sdao.DAOException;

/**
 *
 */
@ManagedBean
@ViewScoped
public class ExportBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    public void export() {

        try(MDRConnection mdr = ConnectionFactory.get(userBean.getUserId())) {
            Exporter exporter = new Exporter(mdr);

            for(MDRElement element : userBean.getExport()) {
                exporter.add(element.getElement());
            }

            for(String namespace : userBean.getExportNamespace()) {
                exporter.add(mdr.get(NamespaceDAO.class).getNamespace(namespace));
            }

            Export exp = exporter.generateExport();

            String serialized = JAXBUtil.marshall(exp, JAXBContext.newInstance(ObjectFactory.class));

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();

            // Some JSF component library or some Filter might have set some headers in the buffer beforehand.
            // We want to get rid of them, else it may collide.
            ec.responseReset();

            // Check http://www.iana.org/assignments/media-types for all types. Use if necessary
            // ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseContentType("application.xml");

            // Set it with the file size. This header is optional. It will work if it's omitted, but the download progress will be unknown.
            ec.setResponseContentLength(serialized.getBytes().length);

            // The Save As popup magic is done here. You can give it any file name you want,
            // this only won't work in MSIE, it will use current request URL as file name instead.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"export-mdr.xml\"");

            OutputStream output = ec.getResponseOutputStream();
            output.write(serialized.getBytes());

            // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
            fc.responseComplete();
        } catch (DAOException | JAXBException | IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }



}
