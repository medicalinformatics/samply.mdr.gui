/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.util.JSFUtil;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * The wizard used to create a new namespace.
 * @author paul
 *
 */
@ManagedBean
@SessionScoped
public class NamespaceWizard extends ElementWizard {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Boolean hidden;

    private String name;

    @Override
    public String validateSlots() {
        return null;
    }

    @Override
    public String validateDefinitions() {
        name = createNewName();
        setStarted(true);
        return BeanValidators.validateDefinitions(getDefinitions()) ? "namespaceverify?faces-redirect=true" : null;
    }

    /**
     * Creates a new identifiable name for this namespace. Tries to use the first designation
     * as template.
     * @return
     */
    private String createNewName() {
        Pattern asciiPattern = Pattern.compile("^[a-z0-9-]+$");
        AdapterDefinition definition = getDefinitions().get(0);
        String input = definition.getDesignation().toLowerCase();

        Matcher matcher = asciiPattern.matcher(input);

        if(matcher.find()) {
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                Namespace namespace = mdr.get(ElementDAO.class).getNamespace(input);

                if(namespace == null) {
                    return input;
                }

                int i = 0;
                while(namespace != null) {
                    ++i;
                    namespace = mdr.get(ElementDAO.class).getNamespace(input + i);
                }

                return input + i;
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            input = "mdr";
            Namespace namespace = mdr.get(ElementDAO.class).getNamespace(input);

            if(namespace == null) {
                return input;
            }

            int i = 0;
            while(namespace != null) {
                ++i;
                namespace = mdr.get(ElementDAO.class).getNamespace(input + i);
            }

            return input + i;
        } catch (DAOException e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Override
    protected ElementType getElementType() {
        return null;
    }

    @Override
    public List<WizardStep> getSteps() {
        return Arrays.asList(new WizardStep(JSFUtil.getString("definitions"), 1, "/user/namespacedefinitions.xhtml"),
                new WizardStep(JSFUtil.getString("verification"), 2, "/user/namespaceverify.xhtml"));
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public String reset() {
        super.reset();
        hidden = false;
        return "/index?faces-redirect=true";
    }

    public String finish() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            Namespace namespace = new Namespace();
            namespace.setCreatedBy(getUserBean().getUserId());
            namespace.setName(name);
            namespace.setHidden(hidden);

            mdr.get(ElementDAO.class).saveElement(namespace);
            for(AdapterDefinition def : getDefinitions()) {
                Definition d = Mapper.convert(def);
                d.setElementId(namespace.getId());
                mdr.get(DefinitionDAO.class).saveNamespaceDefinition(d);
            }
            mdr.commit();

            getUserBean().reloadNamespaces(mdr);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        reset();
        return "/index?faces-redirect=true";
    }

    @Override
    public Boolean getReleaseDraftButtons() {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String template(String urn) {
        return null;
    }

}
