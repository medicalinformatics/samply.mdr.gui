/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.ClientBuilder;

import de.samply.cadsr.CadsrClient;
import de.samply.cadsr.CadsrSearch;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.lib.Constants.Source;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * The search bean searches for elements that contain a specific text in
 * their definition or designation.
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class SearchBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The search test. Definitions and designations will be searched for this string.
     */
    private String searchText;

    /**
     * The currently selected element types that will be searched for
     */
    private ElementType[] selectedTypes = new ElementType[] {ElementType.DATAELEMENT, ElementType.DATAELEMENTGROUP, ElementType.RECORD};

    /**
     * The currently selected statuses that will be searched for
     */
    private Status[] selectedStatuses = new Status[] {Status.RELEASED};

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * A list of all available namespaces with results, excluding the writable namespaces
     */
    private List<MDRNamespace> namespaces;

    /**
     * The currently selected source
     */
    private Source source = Source.LOCALMDR;

    /**
     * A list of all writable namespaces with results
     */
    private List<MDRNamespace> myNamespaces;

    /**
     * The list of all available results.
     */
    private List<MDRElement> results = new ArrayList<>();

    /**
     * The current search in the CaDSR
     */
    private CadsrSearch cadsr = new CadsrSearch();

    /**
     * The list of all currently selected elements. It is a subset of results.
     */
    private List<MDRElement> activeElements;

    /**
     * The currently selected namespace.
     */
    private String selectedNamespace = null;

    /**
     * The type of this search.
     */
    private SearchType searchType = SearchType.ELEMENTS;

    /**
     * Searches for elements and updated the results array.
     * @return
     */
    public String updateResults() {
        /**
         * If the search text is a valid urn, show him the details page immediately.
         */
        if(ScopedIdentifier.isURN(searchText)) {
            return "/detail?faces-redirect=true&urn=" + searchText;
        } else if(searchType == SearchType.ELEMENTS) {
            /**
             * Search for elements with appropriate filters.
             */
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                results = Mapper.convertElements(mdr.get(IdentifiedDAO.class).findElements(searchText, null, selectedTypes, selectedStatuses,
                        new HashMap<String, String>()),
                        getUserBean().getSelectedLanguage(), true);

                selectedNamespace = null;

                updateElements();

                /**
                 * Load all namespaces. This is inefficient, but currently there is no other way to do it.
                 */
                HashSet<String> loaded = new HashSet<>();

                namespaces = new ArrayList<>();
                myNamespaces = new ArrayList<>();

                /**
                 * TODO: Those queries are inefficient...
                 */
                List<MDRNamespace> writable = getUserBean().getWritableNamespaces();

                /**
                 * For each element, check if the namespace has been loaded already, and if it hasn't, load it.
                 */
                for(MDRElement element : results) {
                    String namespace = element.getElement().getScoped().getNamespace();
                    if(!loaded.contains(namespace)) {
                        MDRNamespace ns = Mapper.convertNamespace(mdr.get(NamespaceDAO.class).getNamespace(namespace),
                                userBean.getSelectedLanguage());

                        if(writable.contains(ns)) {
                            myNamespaces.add(ns);
                        } else {
                            namespaces.add(ns);
                        }
                        loaded.add(namespace);
                    }
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }
        } else if(searchType == SearchType.NAMESPACES){
            /**
             * Search for namespaces.
             */
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                namespaces = Mapper.convertNamespaces(mdr.get(NamespaceDAO.class).findNamespaces(searchText),
                        userBean.getSelectedLanguage());
                activeElements = Collections.emptyList();
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(namespaces, MDRNamespace.nameComparator);
        return null;
    }

    /**
     * Updates the list of the currently selected elements.
     * @return
     */
    public String updateElements() {
        if(searchType == SearchType.ELEMENTS) {
            /**
             * If the user selected "all namespaces", just show the results.
             */
            if(StringUtil.isEmpty(selectedNamespace)) {
                activeElements = results;
            } else {
                /**
                 * Otherwise search for elements in the result set, that are in the selected namespace.
                 */
                try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                    MDRNamespace activeNamespace = Mapper.convertNamespace(mdr.get(NamespaceDAO.class).getNamespace(selectedNamespace),
                            userBean.getSelectedLanguage());

                    activeElements = new ArrayList<>();

                    for(MDRElement element : results) {
                        if(element.getElement().getScoped().getNamespaceId() == activeNamespace.getId()) {
                            activeElements.add(element);
                        }
                    }
                } catch (DAOException e) {
                    e.printStackTrace();
                }
            }
        } else if(searchType == SearchType.NAMESPACES) {
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                activeElements = Mapper.convertElements(mdr.get(IdentifiedDAO.class).getRootElements(selectedNamespace),
                        userBean.getSelectedLanguage(), true);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String updateCadsrResults() {
        CadsrClient client = new CadsrClient(ClientBuilder.newClient());
        try {
            cadsr = client.search(searchText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String loadNext() {
        CadsrClient client = new CadsrClient(ClientBuilder.newClient());
        try {
            client.loadNext(cadsr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public List<MDRElement> getResults() {
        return results;
    }


    public Status[] getStatuses() {
        return Status.values();
    }

    public ElementType[] getSelectedTypes() {
        return selectedTypes;
    }

    public void setSelectedTypes(ElementType[] selectedTypes) {
        this.selectedTypes = selectedTypes;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Status[] getSelectedStatuses() {
        return selectedStatuses;
    }

    /**
     * @param selectedStatuses
     */
    public void setSelectedStatuses(Status[] selectedStatuses) {
        this.selectedStatuses = selectedStatuses;
    }

    /**
     * @return the source
     */
    public Source getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(Source source) {
        this.source = source;
    }

    /**
     * @return the cadsr
     */
    public CadsrSearch getCadsr() {
        return cadsr;
    }

    /**
     * @param cadsr the cadsr to set
     */
    public void setCadsr(CadsrSearch cadsr) {
        this.cadsr = cadsr;
    }

    /**
     * @return the activeElements
     */
    public List<MDRElement> getActiveElements() {
        return activeElements;
    }

    /**
     * @param activeElements the activeElements to set
     */
    public void setActiveElements(List<MDRElement> activeElements) {
        this.activeElements = activeElements;
    }

    /**
     * @return the namespaces
     */
    public List<MDRNamespace> getNamespaces() {
        return namespaces;
    }

    /**
     * @param namespaces the namespaces to set
     */
    public void setNamespaces(List<MDRNamespace> namespaces) {
        this.namespaces = namespaces;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<MDRElement> results) {
        this.results = results;
    }

    /**
     * @return the searchType
     */
    public SearchType getSearchType() {
        return searchType;
    }

    /**
     * @param searchType the searchType to set
     */
    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public SearchType[] getSearchTypes() {
        return SearchType.values();
    }

    /**
     * Returns an array of *searchable* element types.
     * @return
     */
    public ElementType[] getTypes() {
        return new ElementType[] {ElementType.DATAELEMENT, ElementType.DATAELEMENTGROUP, ElementType.RECORD, ElementType.CATALOG};
    }

    /**
     * @return the selectedNamespace
     */
    public String getSelectedNamespace() {
        return selectedNamespace;
    }

    /**
     * @param selectedNamespace the selectedNamespace to set
     */
    public void setSelectedNamespace(String selectedNamespace) {
        this.selectedNamespace = selectedNamespace;
    }

    /**
     * @return the myNamespaces
     */
    public List<MDRNamespace> getMyNamespaces() {
        return myNamespaces;
    }

    /**
     * @param myNamespaces the myNamespaces to set
     */
    public void setMyNamespaces(List<MDRNamespace> myNamespaces) {
        this.myNamespaces = myNamespaces;
    }

    public static enum SearchType {ELEMENTS, NAMESPACES};

}
