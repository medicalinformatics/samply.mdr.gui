/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.NamespacePermission;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.UserDAO;
import de.samply.mdr.gui.DetailedMDRElement;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.gui.NamespacePermissionDTO;
import de.samply.mdr.gui.SlotDTO;
import de.samply.mdr.gui.ValueDomainDTO;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.gui.exceptions.ElementNotFoundException;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.Constants.DateRepresentation;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.lib.Constants.TimeRepresentation;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * A mapper that converts the objects from DAOs to objects used in the GUI
 * @author paul
 *
 */
public class Mapper {

    private static final Comparator<MDRElement> elementComparator = new Comparator<MDRElement>() {
        @Override
        public int compare(MDRElement o1, MDRElement o2) {
            if(o1.getElement().getElementType() == o2.getElement().getElementType()) {
                return o1.getDesignation().compareToIgnoreCase(o2.getDesignation());
            } else {
                return o1.getElement().getElementType().compareTo(o2.getElement().getElementType());
            }
        }
    };

    /**
     * Creates a value domain DTO from the given value domain. Uses the MDR connection
     * to get all definitions for the given scoped identifier.
     *
     * @param domain
     * @param mdr
     * @param identifier
     * @return
     * @throws DAOException
     */
    public static ValueDomainDTO convert(ValueDomain domain,
            MDRConnection mdr, ScopedIdentifier identifier, Language priority) throws DAOException {
        ValueDomainDTO valueDomain = new ValueDomainDTO();
        Pattern rangePattern = Pattern.compile("^(?:(.*)<=)?x(?:<=(.*))?$");

        if(domain instanceof EnumeratedValueDomain) {
            /**
             * In case of an enumerated value domain, get all permissible values
             */
            valueDomain.setDatatype(DatatypeField.LIST);
            valueDomain.getPermittedValues().clear();

            for(Element element : mdr.get(ElementDAO.class).getPermissibleValues(domain.getId())) {
                PermissibleValue value = (PermissibleValue) element;
                AdapterPermissibleValue v = new AdapterPermissibleValue();
                v.setValue(value.getPermittedValue());
                v.setDefinitions(convert(mdr.get(DefinitionDAO.class).getDefinitions(value.getId(),
                        identifier.getId())));
                v.setId(value.getId());
                valueDomain.getPermittedValues().add(v);
            }
        } else if(domain instanceof CatalogValueDomain) {
            /**
             * In case of a catalog value domain, first get all catalog codes and
             * then all permissible values.
             */
            valueDomain.setDatatype(DatatypeField.CATALOG);

            IdentifiedElement catalog = mdr.get(IdentifiedDAO.class).getElement(((CatalogValueDomain) domain).getCatalogScopedIdentifierId());

            ScopedIdentifier scopedIdentifier = catalog.getScoped();
            valueDomain.setCatalog(scopedIdentifier.toString());

            valueDomain.setCatalogRoot(new TreeNode(Mapper.convert(catalog, priority)));

            HashMap<Integer, TreeNode> index = new HashMap<>();
            index.put(catalog.getScoped().getId(), valueDomain.getCatalogRoot());

            HashMap<String, TreeNode> urnIndex = new HashMap<>();
            urnIndex.put(valueDomain.getCatalog(), valueDomain.getCatalogRoot());

            /**
             * First put them all in an index.
             */
            for(IdentifiedElement element : mdr.get(IdentifiedDAO.class).getAllSubMembers(valueDomain.getCatalog())) {
                TreeNode node = new TreeNode(Mapper.convert(element, priority));

                index.put(element.getScoped().getId(), node);
                urnIndex.put(element.getScoped().toString(), index.get(element.getScoped().getId()));

                node.setSelected(false);
                node.setState(ChildrenSelectionState.NONE);
            }

            valueDomain.setSubCodes(Mapper.convertElements(mdr.get(IdentifiedDAO.class).getEntries(valueDomain.getCatalog()),
                    priority, false));

            /**
             * Create the tree using the nodes.
             */
            for(HierarchyNode node : mdr.getHierarchyNodes(valueDomain.getCatalog())) {
                index.get(node.getSuperId()).getChildren().add(index.get(node.getSubId()));
                index.get(node.getSubId()).getParents().add(index.get(node.getSuperId()));
            }

            valueDomain.setUrnIndex(urnIndex);
            valueDomain.setIndex(index);

            for(IdentifiedElement element : mdr.get(IdentifiedDAO.class).getPermissibleCodes(domain)) {
                urnIndex.get(element.getScoped().toString()).setSelected(true);
            }

            valueDomain.updateTreeState();
        } else if(domain instanceof DescribedValueDomain) {
            DescribedValueDomain described = (DescribedValueDomain) domain;
            Matcher matcher = null;

            switch(described.getValidationType()) {
            case BOOLEAN:
                valueDomain.setDatatype(DatatypeField.BOOLEAN);
                break;

            case DATE:
                valueDomain.setDatatype(DatatypeField.DATE);
                valueDomain.setWithDays(described.getFormat().contains("DD"));
                valueDomain.setDateRepresentation(DateRepresentation.parseValidationData(described.getValidationData()));
                break;

            case DATETIME:
                valueDomain.setDatatype(DatatypeField.DATETIME);
                valueDomain.setWithDays(described.getFormat().contains("DD"));
                valueDomain.setWithSeconds(described.getFormat().contains("ss"));
                valueDomain.setDateRepresentation(DateRepresentation.parseValidationData(described.getValidationData()));
                valueDomain.setTimeRepresentation(TimeRepresentation.parseValidationData(described.getValidationData()));
                break;

            case TIME:
                valueDomain.setDatatype(DatatypeField.TIME);
                valueDomain.setWithSeconds(described.getFormat().contains("ss"));
                valueDomain.setTimeRepresentation(TimeRepresentation.parseValidationData(described.getValidationData()));
                break;

            case NONE:
                if(described.getMaxCharacters() > 0) {
                    valueDomain.setUseMaxLength(true);
                    valueDomain.setMaxLength(described.getMaxCharacters());
                }

                switch(described.getDatatype()) {
                case "DATE":
                    valueDomain.setDatatype(DatatypeField.DATE);
                    valueDomain.setWithDays(true);
                    break;

                case "DATETIME":
                    valueDomain.setDatatype(DatatypeField.DATETIME);
                    valueDomain.setWithDays(true);
                    valueDomain.setWithSeconds(true);
                    break;

                case "NUMBER":
                case "INTEGER":
                case "INT":
                    valueDomain.setDatatype(DatatypeField.INTEGER);
                    valueDomain.setWithinRange(false);
                    break;

                default:
                    valueDomain.setDatatype(DatatypeField.STRING);
                    valueDomain.setUseRegex(false);
                    break;
                }

                break;

            case REGEX:
                valueDomain.setDatatype(DatatypeField.STRING);
                valueDomain.setUseRegex(true);
                valueDomain.setFormat(described.getValidationData());

                if(described.getMaxCharacters() > 0) {
                    valueDomain.setUseMaxLength(true);
                    valueDomain.setMaxLength(described.getMaxCharacters());
                }
                break;

            case FLOAT:
                valueDomain.setDatatype(DatatypeField.FLOAT);
                valueDomain.setWithinRange(false);
                valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
                break;

            case FLOATRANGE:
                valueDomain.setDatatype(DatatypeField.FLOAT);
                valueDomain.setWithinRange(true);
                valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
                matcher = rangePattern.matcher(described.getValidationData());

                if(matcher.find()) {
                    if(matcher.group(1) != null) {
                        valueDomain.setRangeFrom(Float.parseFloat(matcher.group(1)));
                        valueDomain.setUseRangeFrom(true);
                    } else {
                        valueDomain.setUseRangeFrom(false);
                    }

                    if(matcher.group(2) != null) {
                        valueDomain.setRangeTo(Float.parseFloat(matcher.group(2)));
                        valueDomain.setUseRangeTo(true);
                    } else {
                        valueDomain.setUseRangeTo(false);
                    }
                } else {
                    valueDomain.setUseRangeFrom(false);
                    valueDomain.setUseRangeTo(false);
                    valueDomain.setWithinRange(false);
                }
                break;

            case INTEGER:
                valueDomain.setDatatype(DatatypeField.INTEGER);
                valueDomain.setWithinRange(false);
                valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
                break;

            case INTEGERRANGE:
                valueDomain.setDatatype(DatatypeField.INTEGER);
                valueDomain.setWithinRange(true);
                valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
                matcher = rangePattern.matcher(described.getValidationData());

                if(matcher.find()) {
                    if(matcher.group(1) != null) {
                        valueDomain.setRangeFrom(Integer.parseInt(matcher.group(1)));
                        valueDomain.setUseRangeFrom(true);
                    } else {
                        valueDomain.setUseRangeFrom(false);
                    }

                    if(matcher.group(2) != null) {
                        valueDomain.setRangeTo(Integer.parseInt(matcher.group(2)));
                        valueDomain.setUseRangeTo(true);
                    } else {
                        valueDomain.setUseRangeTo(false);
                    }
                } else {
                    valueDomain.setUseRangeFrom(false);
                    valueDomain.setUseRangeTo(false);
                    valueDomain.setWithinRange(false);
                }
                break;

            default:
                break;

            }
        }

        return valueDomain;
    }

    /**
     * Convert a DefinitionDTO object into a storable Definition
     * @param definition
     * @return
     */
    public static Definition convert(AdapterDefinition definition) {
        Definition def = new Definition();
        def.setDefinition(definition.getDefinition());
        def.setDesignation(definition.getDesignation());
        def.setLanguage(definition.getLanguage().getName());
        return def;
    }

    /**
     * Convert a value domain DTO into a storable value domain object.
     * @param valueDomain
     * @return
     */
    public static ValueDomain convert(ValueDomainDTO valueDomain) {
        ValueDomain domain = null;

        if(valueDomain.getDatatype() == DatatypeField.LIST) {
            domain = new EnumeratedValueDomain();
            domain.setDatatype("enumerated");
            domain.setFormat("enumerated");
            domain.setUnitOfMeasure(null);
        } else if(valueDomain.getDatatype() == DatatypeField.CATALOG) {
            CatalogValueDomain cDomain = new CatalogValueDomain();
            cDomain.setCatalogScopedIdentifierId(valueDomain.getCatalogRoot().getElement().getElement().getScoped().getId());
            domain = cDomain;
            domain.setDatatype("catalog");
            domain.setFormat("catalog");
            domain.setUnitOfMeasure(null);

        } else {
            DescribedValueDomain described = new DescribedValueDomain();

            switch(valueDomain.getDatatype()) {
            case BOOLEAN:
                described.setDatatype("BOOLEAN");
                described.setDescription("(true|false|yes|no|f|t)");
                described.setFormat(described.getDescription());
                described.setValidationType(ValidationType.BOOLEAN);
                described.setValidationData(described.getFormat());
                described.setMaxCharacters(5);
                described.setUnitOfMeasure(null);
                break;

            case FLOAT:
            case INTEGER:
                described.setDatatype(valueDomain.getDatatype().toString());
                described.setMaxCharacters(valueDomain.getRangeTo().toString().length());
                if(valueDomain.getWithinRange()) {
                    described.setValidationType(ValidationType.valueOf(valueDomain.getDatatype().toString() + "RANGE"));
                    described.setValidationData((valueDomain.getUseRangeFrom() ? valueDomain.getRangeFrom() + "<=" : "")
                            + "x" + (valueDomain.getUseRangeTo() ? "<=" + valueDomain.getRangeTo() : ""));
                    described.setFormat(described.getValidationData());
                } else {
                    described.setValidationType(ValidationType.valueOf(valueDomain.getDatatype().toString()));
                    described.setValidationData("x");
                    described.setFormat(described.getValidationData());
                }
                described.setUnitOfMeasure(valueDomain.getUnitOfMeasure());
                described.setDescription(described.getFormat());
                break;

            case STRING:
                described.setDescription(valueDomain.getFormat());
                described.setFormat(valueDomain.getFormat());
                described.setDatatype("STRING");
                described.setUnitOfMeasure(null);

                if(valueDomain.getUseMaxLength()) {
                    described.setMaxCharacters(valueDomain.getMaxLength());
                } else {
                    described.setMaxCharacters(0);
                }

                if(valueDomain.getUseRegex()) {
                    described.setValidationType(ValidationType.REGEX);
                    described.setValidationData(valueDomain.getFormat());
                } else {
                    described.setValidationType(ValidationType.NONE);
                    described.setValidationData(null);
                }

                break;

            case DATE:
                /**
                 * If the date is in the local date, always set with days.
                 */
                if(valueDomain.getDateRepresentation() == DateRepresentation.LOCAL_DATE) {
                    valueDomain.setWithDays(true);
                }
                described.setFormat(getDateFormat(valueDomain));
                described.setDatatype("DATE");
                described.setUnitOfMeasure(null);
                described.setMaxCharacters(described.getFormat().length());
                described.setDescription(described.getFormat());
                described.setValidationType(ValidationType.DATE);
                described.setValidationData(getValidationData(valueDomain));
                break;

            case DATETIME:
                /**
                 * Datetime without days does *not* make sense
                 */
                valueDomain.setWithDays(true);

                described.setFormat(getDateFormat(valueDomain) + " " + getTimeFormat(valueDomain));
                described.setDatatype("DATETIME");
                described.setUnitOfMeasure(null);
                described.setMaxCharacters(described.getFormat().length());
                described.setDescription(described.getFormat());
                described.setValidationType(ValidationType.DATETIME);
                described.setValidationData(getValidationData(valueDomain));
                break;

            case TIME:
                described.setFormat(getTimeFormat(valueDomain));
                described.setDatatype("TIME");
                described.setUnitOfMeasure(null);
                described.setMaxCharacters(described.getFormat().length());
                described.setDescription(described.getFormat());
                described.setValidationType(ValidationType.TIME);
                described.setValidationData(getValidationData(valueDomain));
                break;

            default:
                break;
            }

            domain = described;
        }

        return domain;
    }

    /**
     * Converts the validation data for the given value domain.
     * @param valueDomain
     * @return
     */
    private static String getValidationData(ValueDomainDTO valueDomain) {
        if(valueDomain.getDatatype() == DatatypeField.DATE) {
            return valueDomain.getDateRepresentation().toString() + (valueDomain.getWithDays() ? "_WITH_DAYS" : "");
        }

        /**
         * In case of the date format it does not make sense to disable the days.
         */
        if(valueDomain.getDatatype() == DatatypeField.DATETIME) {
            return valueDomain.getDateRepresentation().toString() + "_WITH_DAYS" +
                    ";" + valueDomain.getTimeRepresentation().toString() + (valueDomain.getWithSeconds() ? "_WITH_SECONDS" : "");
        }

        if(valueDomain.getDatatype() == DatatypeField.TIME) {
            return valueDomain.getTimeRepresentation().toString() + (valueDomain.getWithSeconds() ? "_WITH_SECONDS" : "");
        }

        return null;
    }

    private static String getTimeFormat(ValueDomainDTO valueDomain) {
        List<String> parts = new ArrayList<String>();
        parts.add("hh");
        parts.add("mm");

        if(valueDomain.getWithSeconds()) {
            parts.add("ss");
        }

        return StringUtil.join(parts, ":");
    }

    /**
     * Returns the selected representation format as String
     * @param valueDomain
     * @return
     */
    private static String getDateFormat(ValueDomainDTO valueDomain) {
        switch(valueDomain.getDateRepresentation()) {
        case DIN_5008:
            if(valueDomain.getWithDays()) {
                return "DD.MM.YYYY";
            } else {
                return "MM.YYYY";
            }

        case LOCAL_DATE:
            return "YYYY-MM-DD";

        default:
        case ISO_8601:
            if(valueDomain.getWithDays()) {
                return "YYYY-MM-DD";
            } else {
                return "YYYY-MM";
            }
        }
    }

    /**
     * Converts a list of definitions into their DTO counterparts
     * @param definitions
     * @return
     */
    public static List<AdapterDefinition> convert(List<Definition> definitions) {
        List<AdapterDefinition> target = new ArrayList<>();
        for(Definition definition : definitions) {
            AdapterDefinition def = new AdapterDefinition();
            def.setDefinition(definition.getDefinition());
            def.setDesignation(definition.getDesignation());
            def.setLanguage(Language.valueOf(definition.getLanguage().toUpperCase()));
            target.add(def);
        }
        return target;
    }

    /**
     * Creates a ValueDomainDTO for the given identified element.
     * @param element
     * @param mdr
     * @param priority
     * @return
     * @throws DAOException
     */
    public static ValueDomainDTO createValueDomain(IdentifiedElement element, MDRConnection mdr, Language priority) throws DAOException {
        DataElement de = (DataElement) element.getElement();
        return convert((ValueDomain) mdr.get(ElementDAO.class).getElement(de.getValueDomainId()),
                mdr, element.getScoped(), priority);
    }

    /**
     * Convert the given slots into SlotDTOs
     * @param slots
     * @return
     */
    public static List<SlotDTO> convertSlots(List<Slot> slots) {
        List<SlotDTO> target = new ArrayList<>();

        for(de.samply.mdr.dal.dto.Slot s : slots) {
            SlotDTO slot = new SlotDTO();
            slot.setName(s.getKey());
            slot.setValue(s.getValue());
            target.add(slot);
        }

        return target;
    }

    /**
     * Convert the list of namespaces to MDRNamespaces with the given language.
     * @param namespaces
     * @param priority
     * @return
     */
    public static List<MDRNamespace> convertNamespaces(List<DescribedElement> namespaces, Language priority) {
        List<MDRNamespace> target = new ArrayList<>();

        for(DescribedElement n : namespaces) {
            target.add(convertNamespace(n, priority));
        }

        return target;
    }

    /**
     * Converts a namespace into an MDRNamespace
     * @param n
     * @param priority
     * @return
     */
    public static MDRNamespace convertNamespace(DescribedElement n, Language priority) {
        return new MDRNamespace(n, priority);
    }

    public static List<MDRElement> convertElements(List<IdentifiedElement> elements, Language priority, Boolean sort) {
        List<MDRElement> target = new ArrayList<>();
        for(IdentifiedElement e : elements) {
            target.add(Mapper.convert(e, priority));
        }

        if(sort) {
            Collections.sort(target, elementComparator);
        }
        return target;
    }

    public static DetailedMDRElement convert(IdentifiedElement element,
            List<SlotDTO> slots, List<ScopedIdentifier> versions,
            MDRConnection mdr, Language priority) throws DAOException {

        if(element == null) {
            throw new ElementNotFoundException();
        }

        DetailedMDRElement detailed = new DetailedMDRElement(element, priority);
        detailed.setSlots(slots);
        detailed.setVersions(versions);

        String urn = element.getScoped().toString();

        switch(element.getElementType()) {
        case DATAELEMENT:
            detailed.setValueDomain(createValueDomain(detailed.getElement(), mdr, priority));
            break;

        case DATAELEMENTGROUP:
            detailed.setMembers(convertElements(mdr.get(IdentifiedDAO.class).getSubMembers(urn), priority, true));
            break;

        case RECORD:
            detailed.setMembers(convertElements(mdr.get(IdentifiedDAO.class).getEntries(urn), priority, false));
            break;

        case CATALOG:
        case CODE:
            detailed.setMembers(convertElements(mdr.get(IdentifiedDAO.class).getEntries(urn), priority, false));
            break;

        default:
            break;
        }

        return detailed;
    }

    public static MDRElement convert(IdentifiedElement element, Language priority) {
        if(element == null) {
            throw new RuntimeException("ELEMENT NOT FOUND!");
        }
        return new MDRElement(element, priority);
    }

    public static List<NamespacePermissionDTO> convert(List<NamespacePermission> permissions, MDRConnection mdr) throws DAOException {
        List<NamespacePermissionDTO> target = new ArrayList<>();
        for(NamespacePermission p : permissions) {
            NamespacePermissionDTO dto = new NamespacePermissionDTO();
            User user = mdr.get(UserDAO.class).getUser(p.getUserId());
            dto.setEmail(user.getEmail());
            dto.setRealName(user.getRealName());
            dto.setUserId(p.getUserId());
            dto.setExternalLabel(user.getExternalLabel());
            dto.setPermission(p.getPermission());
            target.add(dto);
        }

        return target;
    }

    public static List<NamespacePermission> convertPermissions(List<NamespacePermissionDTO> permissions) {
        ArrayList<NamespacePermission> target = new ArrayList<>();
        for(NamespacePermissionDTO dto : permissions) {
            NamespacePermission permission = new NamespacePermission();
            permission.setPermission(dto.getPermission());
            permission.setUserId(dto.getUserId());
            permission.setNamespaceId(0);
            target.add(permission);
        }
        return target;
    }
}
