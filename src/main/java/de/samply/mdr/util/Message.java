/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 * Helper class to create error messages from bundles
 * @author paul
 *
 */
public class Message {

    public static void addError(String name, Object... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");

        String msg = bundle.getString(name);
        String detail = null;
        try {
            detail = bundle.getString(name + "Detail");
        } catch(MissingResourceException e) {
            detail = msg;
        }

        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, MessageFormat.format(detail, params)));
    }

    public static void addError(String name, boolean keep) {
        add(FacesMessage.SEVERITY_ERROR, name, keep);
    }

    public static void addError(String name) {
        add(FacesMessage.SEVERITY_ERROR, name, false);
    }

    public static void addInfo(String name, boolean keep) {
        add(FacesMessage.SEVERITY_INFO, name, keep);
    }

    public static void addInfo(String name) {
        add(FacesMessage.SEVERITY_INFO, name, false);
    }

    public static void add(Severity severity, String name) {
        add(severity, name, false);
    }

    public static void add(Severity severity, String name, boolean keep) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");

        String msg = bundle.getString(name);
        String detail = null;
        try {
            detail = bundle.getString(name + "Detail");
        } catch(MissingResourceException e) {
            detail = msg;
        }

        addMessage(severity, msg, detail, keep);
    }

    public static void addMessage(Severity severity, String message, String detail, boolean keep) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severity, message, detail));
        if(keep) {
            keepMessages();
        }
    }

    public static void keepMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    public static String getString(String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
        return bundle.getString(name);
    }
}
